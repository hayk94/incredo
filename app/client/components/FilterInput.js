import   React, {Component,PropTypes} from 'react';
         require('./../styles/filterinput.css')


 class inputFilter extends React.Component {
 constructor (props) {
   super(props);
 }


 render() {
   return (
       <div className="filter-input-block">
         <h2>filter</h2>
         <input
             onKeyUp = {(e) => {this.props.handleFilter(e)}}
             type ="text"
             placeholder ="enter name"
         />
      </div>
   );
 }
}

export default  inputFilter;

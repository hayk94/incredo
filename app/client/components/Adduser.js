import   React, {Component,PropTypes} from 'react';
         require('./../styles/adduser.css')


 class addUser extends React.Component {
 constructor (props) {
   super(props);
   this.user = {};
 }

getUserInfo(event,field) {
   let value = event.target.value;
   switch(field) {
      case 'name':  this.user.name = value;
      break;
      case 'email' : this.user.email = value;
      break;
      case 'phone': this.user.phone = value;
      break;
   }
}

 render() {
   return (
       <div className="adduser-block">
         <h2>add user</h2>
         <input type="text" placeholder="name" onKeyUp={(e) => {this.getUserInfo(e,'name')}}/>
         <input type="text" placeholder="email" onKeyUp={(e) => {this.getUserInfo(e,'email')}}/>
         <input type="text" placeholder="phone" onKeyUp={(e) => {this.getUserInfo(e,'phone')}}/>
      <button onClick={(e) => {this.props.add(e,this.user)}} > save </button>
      </div>
   );
 }
}

addUser.propTypes = {
   add: PropTypes.function
};

export default  addUser;

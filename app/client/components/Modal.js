import   React, {Component,PropTypes} from 'react';
         require('./../styles/useritem.css')


 class Modal extends React.Component {
 constructor (props) {
   super(props);
   this.state = {
      modalStatus: false,
   }
 }

 componentWillReceiveProps(nextProps) {
    this.setState({
      modalStatus: nextProps.state.modalStatus
   })
 }



 render() {
   return (
      <div className={!this.state.modalStatus ? 'overlay' : 'overlay-visible'}>
            <div className="modal-content">
              <div className="close" onClick={() => {this.props.closeMdl()}}>x</div>
                <input
                   type="text"
                   placeholder={this.props.user == null ? 'name' : this.props.user.name}
                   onKeyUp={(e) => this.props.updateParams(e,'name')}/>
                <input
                   type="text"
                   placeholder={this.props.user == null ? 'email' : this.props.user.email}
                   onKeyUp={(e) => this.props.updateParams(e,'email')}/>
                <input
                   type="text"
                   placeholder={this.props.user == null ? 'phone' : this.props.user.phone}
                   onKeyUp={(e) => this.props.updateParams(e,'phone')}/>
               <button onClick={() => {this.props.saveUser()}}> Save</button>
            </div>
      </div>
   );
 }
}

Modal.propTypes = {
   user: PropTypes.array.isRequired,
   updateParams : PropTypes.function,
   saveUser : PropTypes.function,
   closeMdl : PropTypes.function
};


export default  Modal;

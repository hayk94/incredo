import      React from 'react';
const       PropTypes = require('prop-types');
import      {createContainer} from 'meteor/react-meteor-data';
import      FilterInput from './FilterInput.js';
import      UserItem from './UserItem.js';
import      AddUser from './Adduser.js';
import      Modal    from  './Modal.js';
import     {Users}   from './../../imports/user.js';
require('./../styles/main.css');

const winHeight = window.innerHeight;

 class App extends React.Component {

  constructor (props) {
    super(props);

    this.state = {
      users: [],
      modalStatus: false
    };

    this.editableUser = null;
    this.addUser                  = this.addUser.bind(this);
    this.findUserFromList         = this.findUserFromList.bind(this);
    this.editUser                 = this.editUser.bind(this);
    this.getUpdatedUserParameters = this.getUpdatedUserParameters.bind(this);
    this.saveUser                 = this.saveUser.bind(this);
    this.closeMdl                 = this.closeMdl.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    console.log(nextProps)
    this.setState({users: nextProps.users});
  };

  addUser(event,user) {
    Meteor.call('addUser', user, (error,result) => {
      if(error) {console.log(error)}
      else{
        console.log('result : ' , result);
      }
    })
  };

  findUserFromList(event) {
       let value = event.target.value;
       let items = this.props.users;
       const updatetList = items.filter((user,i) => {
          return user.name.toLowerCase().indexOf(value) > -1
       })
       this.setState({users:updatetList});
  };

  editUser (user) {
    this.editableUser = user;
    this.setState({modalStatus: !this.state.modalStatus});
  };

  getUpdatedUserParameters(event,field) {
    let val = event.target.value;
    switch(field) {
        case  'name':
               this.editableUser.name = val;
            break;
        case  'email':
               this.editableUser.email = val;
            break;
        case  'phone':
               this.editableUser.phone = val;
            break;
        default:
        return
              this.editableUser.user
      }
  };

  saveUser() {
    let user = this.editableUser;
     Meteor.call('updateUser', user._id, user, (error,result) => {
       if(error) {console.log(error)}
       else{
         console.log('result', result);
       }
     })
     this.closeMdl();
  };

  closeMdl () {
    this.editableUser = null;
    this.setState({modalStatus: !this.state.modalStatus});
  };

  render() {
    return (
      <div className="root-app" style={{'height': winHeight}}>
        <Modal state={this.state}
               user={this.editableUser}
               saveUser={this.saveUser}
               updateParams={this.getUpdatedUserParameters}
               closeMdl ={this.closeMdl}/>

             <div className="top">
               <FilterInput handleFilter = {this.findUserFromList}/>
               <AddUser add={this.addUser}/>
               <div className="clearfix"> </div>
             </div>

        {this.state.users.map((user,index) => {
          return <UserItem key={index}
                           user={user}
                           handleEditFn={this.editUser}/>
        })}
      <div className="clearfix"> </div>
      </div>
    )
  }
}
App.propTypes = {
   users: PropTypes.array.isRequired
};

export default createContainer (() => {
  Meteor.subscribe('user_l');
   return {
     users: Users.find({}).fetch(),
   }
   Users.update(userId, { $set: { user:user } });

},App);

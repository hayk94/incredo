
import    React, {Component,PropTypes} from 'react';
          require('./../styles/useritem.css');

 class UserItem extends React.Component {
  constructor (props) {
    super(props);

  }

    render(){
      return (
          <ul className="user-list">
              <li className="user-item" >
                <div className="name">   {this.props.user.name}  </div>
                <div className="email">  {this.props.user.email} </div>
                <div className="phone">  {this.props.user.phone} </div>
                <div className="edit-btn"
                     onClick={ () => {this.props.handleEditFn(this.props.user)}}>
                    edit
                </div>
                <div className="clearfix"></div>
              </li>
            </ul>
      )
    }
}

UserItem.propTypes = {
  user: PropTypes.array.isRequired,
  handleEditFn : PropTypes.function
};

export default UserItem;

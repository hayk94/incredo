import        React from 'react';
import       {Meteor} from 'meteor/meteor';
import       {render} from 'react-dom';
import        ReactDOM from 'react-dom';
import        App from './components/App'


Meteor.startup(() => {
  ReactDOM.render(
        <App/>,
      document.getElementById('render-target')
  );
});

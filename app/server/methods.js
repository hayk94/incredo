import {Users} from './../imports/user.js';

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish('user_l', function tasksPublication() {
    return Users.find();
  });
}

Meteor.methods({

  addUser(user) {
    Users.insert(user);
  },

  updateUser(id,user) {
    Users.update({_id: id},
       {$set: {
          name: user.name,
          email: user.email,
          phone:user.phone
        }
    });
  }

})
